#!/usr/bin/env python
from rc243.report import Report

import csv

report = Report()

with open('tfsa.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)

    for row in reader:
        report.addRow(row)

options = {
    'show_empty': False,
    'format': 'orgtbl',
    'active_years': [
        2016,
        2017,
        2018,
    ]
}

report.print(options)