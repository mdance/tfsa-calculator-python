from .summary import Summary
from .monthly_summary import MonthlySummary

import copy

class YearlySummary(Summary):
    def __init__(self, year):
        super(YearlySummary, self).__init__()

        self.year = year
        self.months = {}

    def calculate(self, calculation):
        self.open = copy.deepcopy(calculation)

        if calculation.excess > self.max_excess:
            self.max_excess = calculation.excess

        months = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        ]

        for key, value in enumerate(months):
            month = key + 1

            summary = MonthlySummary(self, value)

            self.months[value] = summary

            for entry in self.entries:
                if entry.month == month:
                    summary.addEntry(entry)

            calculation = summary.calculate(calculation)

            if calculation.excess > self.max_excess:
                self.max_excess = calculation.excess

            self.penalty += summary.penalty

        self.close = copy.deepcopy(calculation)

        return calculation