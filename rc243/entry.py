from dateutil.parser import parse

import copy

class Entry:
    def __init__(self, date, amount):
        self.date = date

        self.date_parsed = parse(date)
        self.year = self.date_parsed.year
        self.month = self.date_parsed.month
        self.day = self.date_parsed.day

        self.amount = int(amount)
        self.amount = self.amount / 100

        self.calculation = None

    def calculate(self, calculation):
        amount = self.amount

        calculation.balance += amount

        calculation.excess = calculation.balance - calculation.limit

        if calculation.excess < 0:
            calculation.excess = 0

        if calculation.excess > calculation.max:
            calculation.max = calculation.excess

        self.calculation = copy.deepcopy(calculation)

        return calculation