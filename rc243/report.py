from .entry import Entry
from .yearly_summary import YearlySummary
from .calculation import Calculation

from datetime import datetime
from babel.numbers import format_currency
from tabulate import tabulate

class Report:
    def __init__(self):
        self.entries = []
        self.summaries = {}
        self.penalty = 0

    def addRow(self, row):
        entry = Entry(row['Date'], row['Amount'])

        self.entries.append(entry)

    def defaults(self):
        output = {}

        # https://en.wikipedia.org/wiki/Tax-Free_Savings_Account
        output['limits'] = {
            2009: 5000,
            2010: 5000,
            2011: 5000,
            2012: 5000,
            2013: 5500,
            2014: 5500,
            2015: 10000,
            2016: 5500,
            2017: 5500,
            2018: 5500,
        }

        now = datetime.now()
        year = now.year

        output['active_years'] = []

        for year in range(2009, year):
            output['active_years'].append(year)

        return output

    def calculate(self, options):
        active_years = sorted(options['active_years'])

        calculation = Calculation(0, 0, 0, 0)

        for year in active_years:
            try:
                limit = options['limits'][year]
            except:
                raise ValueError(f"A limit could not be retrieved for {year}")

            calculation.limit += limit
            calculation.excess -= limit

            summary = YearlySummary(year)

            self.summaries[year] = summary

            for entry in self.entries:
                if entry.year == year:
                    summary.addEntry(entry)

            calculation = summary.calculate(calculation)

            self.penalty += summary.penalty

            # if calculation.excess > 0:
            #     calculation.limit = -calculation.excess

    def print(self, options):
        defaults = self.defaults()

        options = dict(list(defaults.items()) + list(options.items()))

        self.calculate(options)

        active_years = sorted(options['active_years'])

        headers = [
            'Date',
            'Contribution',
            'Withdrawal',
            'Balance',
            'Limit',
            'Excess',
        ]

        format = options['format']

        currency = 'CAD'
        currency_format = u'#,###.##'

        for year in active_years:
            summary = self.summaries[year]

            if options['show_empty']:
                display = True
            else:
                display = False

            if summary.max_excess > 0:
                display = True

            if display:
                print(f"{year}")

                if summary.penalty:
                    penalty = format_currency(summary.penalty, currency, currency_format)

                    print(f"Penalty: {penalty}\n")

                for key, month in summary.months.items():
                    if options['show_empty']:
                        display = True
                    else:
                        display = False

                    if month.max_excess > 0:
                        display = True

                    if display:
                        print(f"{month.month}\n")

                        rows = []

                        row = [
                            1,
                            '',
                            '',
                            format_currency(month.open.balance, currency, currency_format),
                            format_currency(month.open.limit, currency, currency_format),
                            format_currency(month.open.excess, currency, currency_format),
                        ]

                        rows.append(row)

                        for entry in month.entries:
                            contribution = '';
                            withdrawal = '';

                            if entry.amount >= 0:
                                contribution = format_currency(entry.amount, currency, currency_format)
                            else:
                                withdrawal = format_currency(abs(entry.amount), currency, currency_format)

                            row = [
                                entry.day,
                                contribution,
                                withdrawal,
                                format_currency(entry.calculation.balance, currency, currency_format),
                                format_currency(entry.calculation.limit, currency, currency_format),
                                format_currency(entry.calculation.excess, currency, currency_format),
                            ]

                            rows.append(row)

                        if len(rows) == 0:
                            row = [
                                None,
                                None,
                                None,
                                None,
                                None,
                                None,
                            ]

                            rows.append(row)

                        if month.max_excess > 0:
                            max_excess = format_currency(month.max_excess, currency, currency_format)
                            penalty = format_currency(month.penalty, currency, currency_format)

                            row = [
                                None,
                                None,
                                None,
                                None,
                                None,
                                None,
                            ]

                            rows.append(row)

                            row = [
                                None,
                                None,
                                None,
                                None,
                                'Max Excess',
                                max_excess,
                            ]

                            rows.append(row)

                            row = [
                                None,
                                None,
                                None,
                                None,
                                'Penalty (1%)',
                                penalty,
                            ]

                            rows.append(row)

                        print(tabulate(rows, headers, format, numalign="right", stralign="right"), "\n")

        penalty = format_currency(self.penalty, currency, currency_format)

        print(f"Total Penalties: {penalty}")