class Summary:
    def __init__(self):
        self.entries = []
        self.max_excess = 0
        self.penalty = 0

        self.open = None
        self.calculation = None

    def addEntry(self, entry):
        self.entries.append(entry)