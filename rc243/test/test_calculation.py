import pytest
import unittest

from ..calculation import Calculation

class TestCalculation(unittest.TestCase):
    def setUp(self):
        pass

    def test_constructor(self):
        balance = 0
        limit = 0
        excess = 0
        max = 0

        calculation = Calculation(balance, limit, excess, max)

        self.assertEqual(calculation.balance, balance)
        self.assertEqual(calculation.limit, limit)
        self.assertEqual(calculation.excess, excess)
        self.assertEqual(calculation.max, max)
