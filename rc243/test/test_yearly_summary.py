import pytest
import unittest

from datetime import datetime

from ..entry import Entry
from ..calculation import Calculation
from ..yearly_summary import YearlySummary

class TestYearlySummary(unittest.TestCase):
    def setUp(self):
        pass

    def test_constructor(self):
        now = datetime.now()

        object = YearlySummary(now.year)

        self.assertEqual(object.year, now.year)

    def test_calculate(self):
        now = datetime.now()

        object = YearlySummary(now.year)

        amount = 100

        entry = Entry(str(now), amount)

        object.addEntry(entry)

        balance = 0
        limit = 5500
        excess = 0
        max = 0

        calculation = Calculation(balance, limit, excess, max)

        result = object.calculate(calculation)

        self.assertEqual(result.balance, 1.0)
        self.assertEqual(result.excess, 0)
        self.assertEqual(result.limit, limit)
        self.assertEqual(result.max, 0)
        self.assertEqual(object.penalty, 0.0)
        self.assertEqual(object.max_excess, 0)

    def test_calculate_negative_excess(self):
        now = datetime.now()

        object = YearlySummary(now.year)

        amount = 150000

        entry = Entry(str(now), amount)

        object.addEntry(entry)

        balance = 0
        limit = 5500
        excess = 0
        max = 0

        calculation = Calculation(balance, limit, excess, max)

        result = object.calculate(calculation)

        self.assertEqual(result.balance, 1500.0)
        self.assertEqual(result.excess, 0)
        self.assertEqual(result.limit, limit)
        self.assertEqual(result.max, 0)
        self.assertEqual(object.penalty, 0.0)
        self.assertEqual(object.max_excess, 0)

    def test_calculate_positive_excess(self):
        now = datetime.now()

        object = YearlySummary(now.year)

        amount = 1500000

        entry = Entry(str(now), amount)

        object.addEntry(entry)

        balance = 0
        limit = 5500
        excess = 0
        max = 0

        calculation = Calculation(balance, limit, excess, max)

        result = object.calculate(calculation)

        self.assertEqual(result.balance, 15000.0)
        self.assertEqual(result.excess, 9500.0)
        self.assertEqual(result.limit, limit)
        self.assertEqual(result.max, 9500.0)
        self.assertEqual(object.penalty, 855.0)
        self.assertEqual(object.max_excess, 9500.0)


