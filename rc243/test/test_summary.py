import pytest
import unittest

from datetime import datetime

from ..entry import Entry
from ..calculation import Calculation
from ..summary import Summary

class TestSummary(unittest.TestCase):
    def setUp(self):
        pass

    def test_constructor(self):
        entries = []
        max_excess = 0
        penalty = 0

        open = None
        calculation = None

        object = Summary()

        self.assertEqual(object.entries, entries)
        self.assertEqual(object.max_excess, max_excess)
        self.assertEqual(object.penalty, penalty)
        self.assertEqual(object.open, open)
        self.assertEqual(object.calculation, calculation)

    def test_addEntry(self):
        now = datetime.now()
        amount = 100

        entry = Entry(str(now), amount)

        object = Summary()

        object.addEntry(entry)

        entries = []

        entries.append(entry)

        self.assertEqual(object.entries, entries)
