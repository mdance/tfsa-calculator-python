import pytest
import unittest

from datetime import datetime

from ..entry import Entry
from ..calculation import Calculation

class TestEntry(unittest.TestCase):
    def setUp(self):
        pass

    def test_constructor(self):
        now = datetime.now()
        amount = 100

        object = Entry(str(now), amount)

        self.assertEqual(object.year, now.year)
        self.assertEqual(object.month, now.month)
        self.assertEqual(object.day, now.day)
        self.assertEqual(object.amount, (int(amount) / 100))

    def test_calculate(self):
        now = datetime.now()
        amount = 100

        object = Entry(str(now), amount)

        balance = 0
        limit = 5500
        excess = 0
        max = 0

        calculation = Calculation(balance, limit, excess, max)

        result = object.calculate(calculation)

        self.assertEqual(result.balance, 1.0)
        self.assertEqual(result.excess, 0)
        self.assertEqual(result.limit, limit)
        self.assertEqual(result.max, 0)

    def test_calculate_negative_excess(self):
        now = datetime.now()
        amount = 150000

        object = Entry(str(now), amount)

        balance = 0
        limit = 5500
        excess = 0
        max = 0

        calculation = Calculation(balance, limit, excess, max)

        result = object.calculate(calculation)

        self.assertEqual(result.balance, 1500.0)
        self.assertEqual(result.excess, 0)
        self.assertEqual(result.limit, limit)
        self.assertEqual(result.max, 0)

    def test_calculate_positive_excess(self):
        now = datetime.now()
        amount = 1500000

        object = Entry(str(now), amount)

        balance = 0
        limit = 5500
        excess = 0
        max = 0

        calculation = Calculation(balance, limit, excess, max)

        result = object.calculate(calculation)

        self.assertEqual(result.balance, 15000.0)
        self.assertEqual(result.excess, 9500.0)
        self.assertEqual(result.limit, limit)
        self.assertEqual(result.max, 9500.0)
