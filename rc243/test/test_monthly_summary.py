import pytest
import unittest

from datetime import datetime

from ..entry import Entry
from ..calculation import Calculation
from ..yearly_summary import YearlySummary
from ..monthly_summary import MonthlySummary

class TestMonthlySummary(unittest.TestCase):
    def setUp(self):
        pass

    def test_constructor(self):
        now = datetime.now()

        month = now.strftime("%B")

        parent = YearlySummary(now.year)
        object = MonthlySummary(parent, month)

        self.assertEqual(object.parent, parent)
        self.assertEqual(object.month, month)

    def test_calculate(self):
        now = datetime.now()

        month = now.strftime("%B")

        parent = YearlySummary(now.year)
        object = MonthlySummary(parent, month)

        amount = 100

        entry = Entry(str(now), amount)

        object.addEntry(entry)

        balance = 0
        limit = 5500
        excess = 0
        max = 0

        calculation = Calculation(balance, limit, excess, max)

        result = object.calculate(calculation)

        self.assertEqual(result.balance, 1.0)
        self.assertEqual(result.excess, 0)
        self.assertEqual(result.limit, limit)
        self.assertEqual(result.max, 0)
        self.assertEqual(object.penalty, 0.0)
        self.assertEqual(object.max_excess, 0)

    def test_calculate_negative_excess(self):
        now = datetime.now()

        month = now.strftime("%B")

        parent = YearlySummary(now.year)
        object = MonthlySummary(parent, month)

        amount = 150000

        entry = Entry(str(now), amount)

        object.addEntry(entry)

        balance = 0
        limit = 5500
        excess = 0
        max = 0

        calculation = Calculation(balance, limit, excess, max)

        result = object.calculate(calculation)

        self.assertEqual(result.balance, 1500.0)
        self.assertEqual(result.excess, 0)
        self.assertEqual(result.limit, limit)
        self.assertEqual(result.max, 0)
        self.assertEqual(object.penalty, 0.0)
        self.assertEqual(object.max_excess, 0)

    def test_calculate_positive_excess(self):
        now = datetime.now()

        month = now.strftime("%B")

        parent = YearlySummary(now.year)
        object = MonthlySummary(parent, month)

        amount = 1500000

        entry = Entry(str(now), amount)

        object.addEntry(entry)

        balance = 0
        limit = 5500
        excess = 0
        max = 0

        calculation = Calculation(balance, limit, excess, max)

        result = object.calculate(calculation)

        self.assertEqual(result.balance, 15000.0)
        self.assertEqual(result.excess, 9500.0)
        self.assertEqual(result.limit, limit)
        self.assertEqual(result.max, 9500.0)
        self.assertEqual(object.penalty, 95.0)
        self.assertEqual(object.max_excess, 9500.0)

    def test_calculate_example(self):
        now = datetime.now()

        year = now.year

        parent = YearlySummary(year)

        jan = MonthlySummary(parent, 'January')

        jan.addEntry(Entry(f"{year}/01/12", 200000))
        jan.addEntry(Entry(f"{year}/01/20", 250000))
        jan.addEntry(Entry(f"{year}/01/26", 30000))

        balance = 0
        limit = 5500
        excess = 0
        max = 0

        calculation = Calculation(balance, limit, excess, max)

        calculation = jan.calculate(calculation)

        self.assertEqual(calculation.balance, 4800.0)
        self.assertEqual(calculation.excess, 0)
        self.assertEqual(calculation.limit, limit)
        self.assertEqual(calculation.max, 0)
        self.assertEqual(jan.penalty, 0.0)
        self.assertEqual(jan.max_excess, 0)

        feb = MonthlySummary(parent, 'February')

        feb.addEntry(Entry(f"{year}/02/13", 150000))
        feb.addEntry(Entry(f"{year}/02/18", -100000))
        feb.addEntry(Entry(f"{year}/02/23", 90000))

        calculation = feb.calculate(calculation)

        self.assertEqual(calculation.balance, 6200.0)
        self.assertEqual(calculation.excess, 1700.0)
        self.assertEqual(calculation.limit, limit)
        self.assertEqual(calculation.max, 900.0)
        self.assertEqual(feb.penalty, 9.0)
        self.assertEqual(feb.max_excess, 800.0)

