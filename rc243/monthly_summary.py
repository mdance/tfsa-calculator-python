from .summary import Summary

import copy

class MonthlySummary(Summary):
    def __init__(self, parent, month):
        super(MonthlySummary, self).__init__()

        self.parent = parent
        self.month = month

    def calculate(self, calculation):
        self.open = copy.deepcopy(calculation)

        if calculation.excess > self.max_excess:
            self.max_excess = calculation.excess

        for entry in self.entries:
            calculation = entry.calculate(calculation)

            if calculation.excess > self.max_excess:
                self.max_excess = calculation.excess

        self.penalty = self.max_excess * 0.01

        self.close = copy.deepcopy(calculation)

        return calculation
